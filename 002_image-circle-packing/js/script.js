var edgeRadius = 5;
var canvasWidth = 800;
var canvasHeight = 600;
var circlesPerFrame = 1;
var startingRadius = 5;

var seedPoints, output, input, canvas, circles;

function setup() {
	input = createFileInput(handleFile);
	canvas = createCanvas(canvasWidth,canvasHeight);
	resetSetup();
};

function draw() {
	if(seedPoints.length > 0) {
		background(0);
		for(var i=0;i<circlesPerFrame;i++) {		
			var seedPoint = getSeedPoint();
			if(seedPoint) {
				var c = color(seedPoint.r,seedPoint.g,seedPoint.b);
				circles.push(new Circle(seedPoint.x,seedPoint.y,startingRadius,c ));
			} else {
				noLoop();
				console.log('done');
			}
		}	

		circles.forEach(function(circle) {
			circle.checkCollisions(circles);
			circle.update();
			circle.draw();
		});
	}
}

function getSeedPoint() {
	var tries = 0;
	do {
		seedPoint = seedPoints.pop();
		for(var i=0; i < circles.length; i++) {
			if(circles[i].isPointInsideCircle(seedPoint,startingRadius)) {
				seedPoint = null;
				tries++;
				break;
			}
		}
	} while (!seedPoint && seedPoints.length > 0);
	return seedPoint;
}

///////////////////////////////////////////////////////////////////

var resetSetup = function() {
	circles = [];
	seedPoints = [];
	resizeCanvas(canvasWidth,canvasHeight);
	background(0);
	loop();
}

var handleFile = function(file) {
	if (file.type === 'image') {
		resetSetup();
		loadImage(file.data, processImage);
	} else {
		alert('Should be image file!');
	}
}

var processImage = function(img) {
	img.resize(canvasWidth,0);
	resizeCanvas(img.width,img.height);

	seedPoints = retrievePixelArray(img).sort(function(a,b) {
		return a.edgeDistance - b.edgeDistance;
	});
}

var retrievePixelArray = function(img) {
	var pixelArray = [];

	img.loadPixels();
	for(var x = 0; x < img.width; x++) {
		for(var y = 0; y < img.height; y++) {
			var i = (x + y * img.width) * 4;

			var pixel = {
				x: x,
				y: y,
				r: img.pixels[i],
				g: img.pixels[i+1],
				b: img.pixels[i+2]
			};

			var surroundingPixelCount = 0;
			var edgeDistance = 0;

			for(var x2 = pixel.x - edgeRadius; x2 <= pixel.x + edgeRadius; x2+=edgeRadius) {
				for(var y2 = pixel.y - edgeRadius; y2 <= pixel.y + edgeRadius; y2+=edgeRadius) {
					if(x2 >= 0 && x2 <= img.width && y2 >= 0 && y2 <= img.height) {						
						var i2 = (x2 + y2 * img.width) * 4;

						edgeDistance += dist(pixel.r,pixel.g,pixel.b,img.pixels[i2],img.pixels[i2+1],img.pixels[i2+2]);
						surroundingPixelCount++;
					}
				}
			}

			edgeDistance /= surroundingPixelCount;
			pixel.edgeDistance = edgeDistance;

			pixelArray.push(pixel);
		};
	};

	return pixelArray;
}


/////////////////////////////////////////////////////////////

function Circle(_x,_y,_radius,_c) {
	this.x = _x;
	this.y = _y;
	this.radius = _radius;
	this.incr = 1;
	this.strokeWeight = 0;
	this.color = _c;

	this.active = true;

	this.update = function() {
		if(this.active) {
			this.radius += this.incr;
		};
	};

	this.draw = function() {
		fill(this.color);
		noStroke();

		ellipse(this.x,this.y,this.radius*2-this.strokeWeight*2,this.radius*2-this.strokeWeight*2);
	};

	this.checkCollisions = function(circles) {
		if(this.active) {
			this.active = !this.isTouchingEdges();
		}

		if(this.active) {
			for(var i=0; i < circles.length; i++) {
				if(this !== circles[i] && this.isTouchingOtherCircle(circles[i])) {
					this.active = false;
					circles[i].active = false;
					break;
				}
			}
		}
	};

	this.isTouchingEdges = function() {
		return (this.x - this.radius <= 0) || (this.x + this.radius >= width) || (this.y - this.radius <= 0) || (this.y + this.radius >= height);
	};

	this.isTouchingOtherCircle = function(otherCircle) {
		return dist(this.x,this.y,otherCircle.x,otherCircle.y) <= this.radius + otherCircle.radius;
	};

	this.isPointInsideCircle = function(point,offset) {
		return dist(point.x,point.y,this.x,this.y) <= this.radius + offset;
	}
}