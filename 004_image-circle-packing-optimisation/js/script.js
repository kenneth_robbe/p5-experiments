var edgeRadius = 5;
var canvasWidth = 800;
var canvasHeight = 600;
var circleInitialCount = 2500;
var circlePerFrameCount = 50;
var minDiameter = 2;
var maxDiameter = 50;
var offset = 1;
var bgColor = '#000000';
var animated = true;
var circleCount = 0;
var activeCircleCount = 0;
var strokeWght = 0;
var fillActive = true;

var seedPoints, output, input, button, canvas, gridSystem;

function setup() {
	resetSetup();
	input = createFileInput(handleFile);
	button = createButton('save');
  	button.mousePressed(saveFile);
	canvas = createCanvas(canvasWidth,canvasHeight);

	gui = createGui('Set-up');
	sliderRange(0, 2500, 1);
	gui.addGlobals('animated', 'circleInitialCount');
	sliderRange(0, 500, 1);
	gui.addGlobals('circlePerFrameCount');
	sliderRange(0, 10, 0.1);
	gui.addGlobals('minDiameter');	
	sliderRange(0, 300, 0.1);
	gui.addGlobals('maxDiameter');

	gui2 = createGui('Live');
	sliderRange(0, 10, 0.1);
	gui2.addGlobals('offset', 'bgColor', 'strokeWght', 'fillActive');

	noLoop();
};

function draw() {
	if(seedPoints && seedPoints.seedArray && seedPoints.seedArray.length > 0) {
		if(animated) {
			frameStep();
		} else {
			while(seedPoints.seedArray.length > 0) {
				frameStep();
			}
		}
	} else {
		if(circleCount > 0) {			
			console.log('Done! ' + circleCount + ' circles drawn.');
			circleCount = 0;
		}
	}

	background(bgColor);
	if(gridSystem && gridSystem.grid) {
		for(var x = 0; x < gridSystem.columns; x++) {
			for(var y = 0; y < gridSystem.rows; y++) {
				var circles = gridSystem.grid[x][y];
				circles.forEach(function(circle) {
					circle.draw(offset);
				});
			}
		};
	}
}

var frameStep = function() {
	circleCount = 0;
	activeCircleCount = 0;

	for(var x = 0; x < gridSystem.columns; x++) {
		for(var y = 0; y < gridSystem.rows; y++) {
			var circles = gridSystem.grid[x][y];
			circles.forEach(function(circle) {
				circleCount++;
				if(circle.active) {
					activeCircleCount++;
					circle.active = circle.validPosition(gridSystem, true, true);
					circle.update();
				}
			});
		}
	};

	console.log(seedPoints.seedArray.length + ' seed points left. ' + circleCount + ' circles. ' + activeCircleCount + ' active circles. Max Chance: ' + seedPoints.maxValue + '. Frame Rate:' + frameRate());
	addCircles(seedPoints, minDiameter, maxDiameter, gridSystem, circlePerFrameCount);
}


var addCircles = function(seedPoints, minDiameter, maxDiameter, gridSystem, totalCircleCount) {
	var circleCount = 0;
	do {
		var seedPoint = seedPoints.getSeedPoint();
		if(seedPoint) {
			var c = color(seedPoint.r,seedPoint.g,seedPoint.b);
			var circle = new Circle(seedPoint.x, seedPoint.y, minDiameter, maxDiameter, c);

			if(circle.validPosition(gridSystem, true, true)) {
				circleCount++;				
				gridSystem.addElement(circle);
			}
		}
	} while (circleCount < totalCircleCount && seedPoints.seedArray.length > 0);
}

///////////////////////////////////////////////////////////////////

var resetSetup = function() {
	circleCount = 0;
	activeCircleCount = 0;
	seedPoints = [];
	resizeCanvas(canvasWidth,canvasHeight);
	background(bgColor);
}

var handleFile = function(file) {
	if (file.type === 'image') {
		resetSetup();
		console.log('Loading image...');
		loadImage(file.data, processImage);
	} else {
		alert('Should be image file!');
	}
}

var saveFile = function() {
	saveCanvas(canvas, 'output', 'png');
}

var processImage = function(img) {
	console.log('Image Loaded. Finding edges...');
	img.resize(canvasWidth,0);
	resizeCanvas(img.width,img.height);
	gridSystem = new GridSystem(maxDiameter,img.width,img.height);

	var pixelInfo = retrievePixelInfo(img);
	//bgColor = lerpColor(pixelInfo.meanColor,color(0),0.5);

	console.log('Edges found. Finding Seed Points...');
	seedPoints = new SeedPoints(pixelInfo.pixels,'edgeDistance');
	addCircles(seedPoints, minDiameter, maxDiameter, gridSystem, circleInitialCount);

	console.log('Initial seed points found. Drawing ' + circleInitialCount + ' circles...');
	loop();
}

var retrievePixelInfo = function(img) {
	var pixelInfo = {
		pixels: []
	};

	img.loadPixels();
	pixelInfo.pixelCount = img.pixels.length / 4;

	var meanR = 0;
	var meanG = 0;
	var meanB = 0;
	var maxEdgeDistance = dist(0,0,0,255,255,255);

	for(var x = 0; x < img.width; x++) {
		for(var y = 0; y < img.height; y++) {
			var i = (x + y * img.width) * 4;

			var pixel = {
				x: x,
				y: y,
				r: img.pixels[i],
				g: img.pixels[i+1],
				b: img.pixels[i+2]
			};

			meanR += pixel.r;
			meanG += pixel.g;
			meanB += pixel.b;

			var surroundingPixels = [];
			var edgeDistance = 0;

			for(var x2 = pixel.x - edgeRadius; x2 <= pixel.x + edgeRadius; x2+=edgeRadius) {
				for(var y2 = pixel.y - edgeRadius; y2 <= pixel.y + edgeRadius; y2+=edgeRadius) {
					if(x2 >= 0 && x2 < img.width && y2 >= 0 && y2 < img.height) {						
						var i2 = (x2 + y2 * img.width) * 4;

						var otherPixel = {
							x: x2,
							y: y2,
							r: img.pixels[i2],
							g: img.pixels[i2+1],
							b: img.pixels[i2+2]
						};

						surroundingPixels.push(otherPixel)

						edgeDistance += dist(pixel.r,pixel.g,pixel.b,otherPixel.r,otherPixel.g,otherPixel.b);
					}
				}
			}

			edgeDistance /= surroundingPixels.length;
			pixel.edgeDistance = map(edgeDistance,0,maxEdgeDistance,0,1);

			pixelInfo.pixels.push(pixel);
		};
	};
	pixelInfo.meanColor = color(int(meanR/pixelInfo.pixelCount),int(meanG/pixelInfo.pixelCount),int(meanB/pixelInfo.pixelCount));

	return pixelInfo;
}


/////////////////////////////////////////////////////////////

function Circle(_x,_y,_minDiameter,_maxDiameter,_c) {
	this.x = _x;
	this.y = _y;
	this.diameter = _minDiameter;
	this.incr = 1;
	this.color = _c;

	this.active = true;

	this.update = function() {
		if(this.active) {
			this.diameter += this.incr;
			this.active = this.diameter <= _maxDiameter;
		};
	};

	this.draw = function(offset) {
		if(fillActive) {
			fill(this.color);
		} else {
			noFill();
		}
		stroke(this.color);
		strokeWeight(strokeWght);
		var diameter = Math.max(0,this.diameter-offset*2-strokeWght*2);
		ellipse(this.x,this.y,diameter,diameter);
	};

	this.validPosition = function(gridSystem,checkEdges,checkCircles) {
		var active = this.active;

		if(active && checkEdges) {
			active = !this.isTouchingEdges();
		}

		if(active && checkCircles) {
			var gridPos = gridSystem.getGridPos(this.x,this.y);
			for(var x = gridPos.x - 1; x <= gridPos.x + 1; x++) {
				for(var y = gridPos.y - 1; y <= gridPos.y + 1; y++) {
					if(x >= 0 && x < gridSystem.columns && y >= 0 && y < gridSystem.rows) {						
						var circles = gridSystem.grid[x][y];
						for(var i=0; i < circles.length; i++) {
							if(this !== circles[i] && this.isTouchingOtherCircle(circles[i])) {
								active = false;
								circles[i].active = false;
								break;
							}
						}
					}
				}
				if(!active) {break;}
			}
		}

		return active;
	};

	this.isTouchingEdges = function() {
		var dist = this.diameter/2;
		return (this.x - dist <= 0) || (this.x + dist >= width) || (this.y - dist <= 0) || (this.y + dist >= height);
	};

	this.isTouchingOtherCircle = function(otherCircle) {
		return dist(this.x,this.y,otherCircle.x,otherCircle.y) <= this.diameter/2 + otherCircle.diameter/2;
	};
}

/////////////////////////////////////////////////////////////

function SeedPoints(_seedArray,_weightParameter) {	
	this.seedArray = _seedArray;

	this.maxValue = 0;
	for(var i=0; i < this.seedArray.length; i++) {
		this.maxValue = this.seedArray[i][_weightParameter] > this.maxValue ? this.seedArray[i][_weightParameter] : this.maxValue;
	}
	this.maxValue *= 0.8;
	this.step = this.maxValue / this.seedArray.length;

    this.getSeedPoint = function() {
    	var seedPoint = null;
    	do {
    		var	randInt = Math.floor(Math.random() * this.seedArray.length);
    		var randChance = Math.random() * this.maxValue;
    		if(this.seedArray[randInt][_weightParameter] >= randChance) {
    			seedPoint = this.seedArray[randInt];
    			this.seedArray[randInt] = this.seedArray[this.seedArray.length - 1];
    			this.seedArray.pop();
    		}
    		this.maxValue = Math.max(0,this.maxValue-this.step);			
		} while (!seedPoint && this.seedArray.length > 0);

		return seedPoint;
    };
}

/////////////////////////////////////////////////////////////

function GridSystem(_gridSize, _width, _height) {
	this.gridSize = _gridSize;
	this.width = _width;
	this.height = _height;
	this.columns = Math.ceil(this.width / this.gridSize);
	this.rows = Math.ceil(this.height / this.gridSize);
	var _grid = [];

	for(var x = 0; x < this.columns; x++) {
		var column = [];
		for(var y = 0; y < this.rows; y++) {
			var row = [];
			
			column.push(row);
		};
		_grid.push(column);
	}

	

	this.grid = _grid; 

	this.getGridPos = function(x, y) {
		return {
			x: Math.floor(x / this.gridSize),
			y: Math.floor(y / this.gridSize)
		};
	};

	this.addElement = function(element) {
		var gridPos = this.getGridPos(element.x,element.y);
		this.grid[gridPos.x][gridPos.y].push(element);
	}
}