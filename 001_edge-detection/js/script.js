var edgeRadius = 5;
var canvasWidth = 800;
var canvasHeight = 600;
var pixelInfo, output, input, canvas;

function setup() {
	input = createFileInput(handleFile);
	canvas = createCanvas(canvasWidth,canvasHeight);
	background(0);
};

///////////////////////////////////////////////////////////////////

function handleFile(file) {
	if (file.type === 'image') {
		loadImage(file.data, processImage);
	} else {
		alert('Should be image file!');
	}
}

function processImage(img) {
	pixelInfo = [];

	img.resize(canvasWidth,0);
	resizeCanvas(img.width,img.height);

	var output = createImage(img.width,img.height);
	var maxEdgeDistance = dist(0,0,0,255,255,255);

	img.loadPixels();
	output.loadPixels();
	for(var x = 0; x < img.width; x++) {
		for(var y = 0; y < img.height; y++) {
			var i = (x + y * img.width) * 4;

			var pixel = {
				x: x,
				y: y,
				r: img.pixels[i],
				g: img.pixels[i+1],
				b: img.pixels[i+2]
			};

			var surroundingPixelCount = 0;
			var edgeDistance = 0;

			for(var x2 = pixel.x - edgeRadius; x2 <= pixel.x + edgeRadius; x2+=edgeRadius) {
				for(var y2 = pixel.y - edgeRadius; y2 <= pixel.y + edgeRadius; y2+=edgeRadius) {
					if(x2 >= 0 && x2 <= img.width && y2 >= 0 && y2 <= img.height) {						
						var i2 = (x2 + y2 * img.width) * 4;

						edgeDistance += dist(pixel.r,pixel.g,pixel.b,img.pixels[i2],img.pixels[i2+1],img.pixels[i2+2]);
						surroundingPixelCount++;
					}
				}
			}

			edgeDistance /= surroundingPixelCount;
			pixel.edgeDistance = edgeDistance;

			var b = int(map(edgeDistance,0,maxEdgeDistance,0,255));

			output.set(x,y, color(b));

			pixelInfo.push(pixel);
		};
	};

	output.updatePixels();

	image(output, 0, 0);
}