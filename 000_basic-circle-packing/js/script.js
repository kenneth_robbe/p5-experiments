var circles = [];
var circlesPerFrame = 5;
var startingRadius = 3;
var maxTries = 100;

function setup() {
	createCanvas(800,600);
};

function draw() {
	background(255);

	for(var i=0;i<circlesPerFrame;i++) {		
		var seedPoint = getSeedPoint();
		if(seedPoint) {
			circles.push(new Circle(seedPoint.x,seedPoint.y,startingRadius,1));
		} else {
			noLoop();
			console.log('done');
		}
	}	

	circles.forEach(function(circle) {
		circle.checkCollisions(circles);
		circle.update();
		circle.draw();
	});
};

function getSeedPoint() {
	var tries = 0;
	do {
		seedPoint = createVector(random(width),random(height));
		for(var i=0; i < circles.length; i++) {
			if(circles[i].isPointInsideCircle(seedPoint,startingRadius)) {
				seedPoint = null;
				tries++;
				break;
			}
		}
	} while (!seedPoint && tries <= maxTries);
	return seedPoint;
}

function mouseClicked() {
	var mousePos = createVector(mouseX,mouseY);

	for(var i=0; i < circles.length; i++) {
		if(circles[i].isPointInsideCircle(mousePos,0)) {
			circles.splice(i,1);
			break;
		}
	};

	circles.forEach(function(circle) {
		circle.active = true;
	});
}

/////////////////////////////////////////////////////////////

function Circle(_x,_y,_radius,_incr) {
	this.x = _x;
	this.y = _y;
	this.radius = _radius;
	this.incr = _incr;
	this.strokeWeight = 1;

	this.active = true;

	this.update = function() {
		if(this.active) {
			this.radius += this.incr;
		};
	};

	this.draw = function() {
		noFill();
		strokeWeight(this.strokeWeight);
		stroke(0);

		ellipse(this.x,this.y,this.radius*2-this.strokeWeight*2,this.radius*2-this.strokeWeight*2);
	};

	this.checkCollisions = function(circles) {
		if(this.active) {
			this.active = !this.isTouchingEdges();

			for(var i=0; i < circles.length; i++) {
				if(this !== circles[i] && this.isTouchingOtherCircle(circles[i])) {
					this.active = false;
					circles[i].active = false;
					break;
				}
			}
		}
	};

	this.isTouchingEdges = function() {
		return (this.x - this.radius <= 0) || (this.x + this.radius >= width) || (this.y - this.radius <= 0) || (this.y + this.radius >= height);
	};

	this.isTouchingOtherCircle = function(otherCircle) {
		return dist(this.x,this.y,otherCircle.x,otherCircle.y) <= this.radius + otherCircle.radius;
	};

	this.isPointInsideCircle = function(point,offset) {
		return dist(point.x,point.y,this.x,this.y) <= this.radius + offset;
	}
}