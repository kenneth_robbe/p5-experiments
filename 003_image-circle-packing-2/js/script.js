var edgeRadius = 5;
var canvasWidth = 1200;
var canvasHeight = 600;
var numberOfInitialCircles = 500;
var numberOfCirclesPerFrame = 5;
var startingRadius = 0;

var seedPoints, output, input, canvas, circles;

function setup() {
	input = createFileInput(handleFile);
	canvas = createCanvas(canvasWidth,canvasHeight);
	noLoop();
	resetSetup();
};

function draw() {
	background(255);

	if(seedPoints && seedPoints.seedArray && seedPoints.seedArray.length > 0) {
		while(seedPoints.seedArray.length > 0) {
			var drawnCircles = 0;
			do {
				var seedPoint = seedPoints.getSeedPoint();
				if(seedPoint) {
					for(var i=0; i < circles.length; i++) {
						if(circles[i].isPointInsideCircle(seedPoint,startingRadius)) {
							seedPoint = null;
							break;
						}
					}
				}
				if(seedPoint) {
					drawnCircles++;	
					var c = color(seedPoint.r,seedPoint.g,seedPoint.b);
					circles.push(new Circle(seedPoint.x,seedPoint.y,startingRadius,c));
				}
			} while (drawnCircles <= numberOfCirclesPerFrame && seedPoints.seedArray.length > 0);

			circles.forEach(function(circle) {
				circle.checkCollisions(circles);
				circle.update();
				//circle.draw();
			});

			console.log(seedPoints.seedArray.length);
		}

		console.log('done');
		circles.forEach(function(circle) {
			circle.draw();
		});
		noLoop();
		
	}
}

///////////////////////////////////////////////////////////////////

var resetSetup = function() {
	circles = [];
	seedPoints = [];
	resizeCanvas(canvasWidth,canvasHeight);
	background(0);
}

var handleFile = function(file) {
	if (file.type === 'image') {
		resetSetup();
		console.log('Loading image...');
		loadImage(file.data, processImage);
	} else {
		alert('Should be image file!');
	}
}

var processImage = function(img) {
	console.log('Image Loaded. Finding edges...');
	img.resize(canvasWidth,0);
	resizeCanvas(img.width,img.height);

	var pixelArray = retrievePixelArray(img);

	console.log('Edges found. Finding Seed Points...');
	seedPoints = new SeedPoints(pixelArray,'edgeDistance');

	for(var i=0; i < numberOfInitialCircles; i++) {
		var seedPoint = seedPoints.getSeedPoint();
		if(seedPoint) {			
			var c = color(seedPoint.r,seedPoint.g,seedPoint.b);
			circles.push(new Circle(seedPoint.x,seedPoint.y,startingRadius,c));
		} else {
			break;
		}
	}

	console.log('Seed points found. Drawing ' + circles.length + ' circles...');
	loop();
}

var retrievePixelArray = function(img) {
	var pixelArray = [];

	img.loadPixels();
	var maxEdgeDistance = dist(0,0,0,255,255,255);
	for(var x = 0; x < img.width; x++) {
		for(var y = 0; y < img.height; y++) {
			var i = (x + y * img.width) * 4;

			var pixel = {
				x: x,
				y: y,
				r: img.pixels[i],
				g: img.pixels[i+1],
				b: img.pixels[i+2]
			};

			var surroundingPixels = [];
			var edgeDistance = 0;

			for(var x2 = pixel.x - edgeRadius; x2 <= pixel.x + edgeRadius; x2+=edgeRadius) {
				for(var y2 = pixel.y - edgeRadius; y2 <= pixel.y + edgeRadius; y2+=edgeRadius) {
					if(x2 >= 0 && x2 < img.width && y2 >= 0 && y2 < img.height) {						
						var i2 = (x2 + y2 * img.width) * 4;

						var otherPixel = {
							x: x2,
							y: y2,
							r: img.pixels[i2],
							g: img.pixels[i2+1],
							b: img.pixels[i2+2]
						};

						surroundingPixels.push(otherPixel)

						edgeDistance += dist(pixel.r,pixel.g,pixel.b,otherPixel.r,otherPixel.g,otherPixel.b);
					}
				}
			}

			edgeDistance /= surroundingPixels.length;
			pixel.edgeDistance = map(edgeDistance,0,maxEdgeDistance,0,1);

			pixelArray.push(pixel);
		};
	};

	return pixelArray;
}


/////////////////////////////////////////////////////////////

function Circle(_x,_y,_radius,_c) {
	this.x = _x;
	this.y = _y;
	this.radius = _radius;
	this.incr = 1;
	this.strokeWeight = 0;
	this.color = _c;

	this.active = true;

	this.update = function() {
		if(this.active) {
			this.radius += this.incr;
		};
	};

	this.draw = function() {
		fill(this.color);
		noStroke();

		ellipse(this.x,this.y,this.radius*2-this.strokeWeight*2,this.radius*2-this.strokeWeight*2);
	};

	this.checkCollisions = function(circles) {
		/*if(this.active) {
			this.active = !this.isTouchingEdges();
		}*/

		if(this.active) {
			for(var i=0; i < circles.length; i++) {
				if(this !== circles[i] && this.isTouchingOtherCircle(circles[i])) {
					this.active = false;
					circles[i].active = false;
					break;
				}
			}
		}
	};

	this.isTouchingEdges = function() {
		return (this.x - this.radius <= 0) || (this.x + this.radius >= width) || (this.y - this.radius <= 0) || (this.y + this.radius >= height);
	};

	this.isTouchingOtherCircle = function(otherCircle) {
		return dist(this.x,this.y,otherCircle.x,otherCircle.y) <= this.radius + otherCircle.radius;
	};

	this.isPointInsideCircle = function(point,offset) {
		return dist(point.x,point.y,this.x,this.y) <= this.radius + offset;
	}
}

/////////////////////////////////////////////////////////////

function SeedPoints(_seedArray,_weightParameter) {	
	this.seedArray = _seedArray.sort(function(a,b) {
		return a[_weightParameter] - b[_weightParameter];
	});

	this.maxValue = 0;
	for(var i=0; i < this.seedArray.length; i++) {
		this.maxValue = this.seedArray[i][_weightParameter] > this.maxValue ? this.seedArray[i][_weightParameter] : this.maxValue;
	}
	//console.log(this.maxValue);

	/*this.totalWeight = this.seedArray.reduce(function(totalWeight, seedPoint, currentIndex, arr) {
		totalWeight += seedPoint[_weightParameter];
		return totalWeight;
	}, 0);

    this.getSeedPoint = function() {
    	var randomWeight = random(0, this.totalWeight);
	    var weightSum = 0;	    
	     
	    for (var i = 0; i < this.seedArray.length; i++) {
	        weightSum += this.seedArray[i][_weightParameter];
	         
	        if (randomWeight <= weightSum) {
	        	var seedPoint = this.seedArray.splice(i,1)[0];
	        	this.totalWeight -= seedPoint[_weightParameter];
	            return seedPoint;
	        }
	    }
    }.bind(this);*/

    this.getSeedPoint = function() {
    	var seedPoint = null;
    	var tries = 0;
    	do {
    		tries++;
    		var	randInt = int(random(0,this.seedArray.length));	
    		var randChance = random(this.maxValue * 0.5);
    		seedPoint = this.seedArray.splice(randInt,1)[0];

    		if(seedPoint[_weightParameter] < randChance) {
    			seedPoint = null;
    		}
			
		} while (!seedPoint && this.seedArray.length > 0);

		return seedPoint;
    };
}