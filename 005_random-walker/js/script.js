"use strict";

const ORTHOGONAL = [[1,0],[-1,0],[0,1],[0,-1]];
const DIAGONAL = [[1,1],[-1,-1],[-1,1],[1,-1]];
const BOTH = ORTHOGONAL.concat(DIAGONAL);

const RANDOM_WALKER_COUNT = 10;
let randomWalkers = [];

/////////////////////////////////////////////////

let sketch = function(sketch) {

	sketch.setup = function() {
		sketch.createCanvas(500,500);
		sketch.background(0);
		sketch.stroke(255);

		for(var i=0; i<RANDOM_WALKER_COUNT;i++) {	
			randomWalkers.push(new RandomWalker(sketch.random(sketch.width),sketch.random(sketch.height),5,10,ORTHOGONAL));
		}
	};

	sketch.draw = function() {
		//sketch.background(0,50);
		randomWalkers.forEach(function(randomWalker) {
			randomWalker.changeDirection(Math.random());
			randomWalker.step();
			randomWalker.draw(sketch);
		});
	};

}

let myp5 = new p5(sketch);

/////////////////////////////////////////////////

function RandomWalker(_x,_y,_minStep,_maxStep,_allowedDirections) {
	this.pos = new p5.Vector(_x,_y);
	this.prevPos = this.pos.copy();
	this.minStep = _minStep;
	this.maxStep = _maxStep;
	this.dir = new p5.Vector(0,0);
	if(Array.isArray(_allowedDirections) && _allowedDirections.length > 0) {		
		this.allowedDirections = _allowedDirections.map(function(dir) {
			return new p5.Vector(dir[0],dir[1]);
		});
	}
}

RandomWalker.prototype.changeDirection = function(_mag,_step) {
	if(this.allowedDirections) {
		var dir = this.allowedDirections[Math.floor(Math.random()*this.allowedDirections.length)];
	} else {
		var dir = p5.Vector.random2D();
	}

	var mag = typeof _mag == 'number' ? _mag : 1;
	mag = this.minStep + ((this.maxStep - this.minStep) * mag);
	if(typeof _step == 'number') {
		mag = Math.roundTo(mag,_step);
	}

	this.dir.set(dir.setMag(mag));
};

RandomWalker.prototype.step = function() {
	this.prevPos.set(this.pos);
	this.pos.add(this.dir);
};

RandomWalker.prototype.draw = function(_sketch) {
	_sketch.line(this.prevPos.x,this.prevPos.y,this.pos.x,this.pos.y);
};

/////////////////////////////////////////////////

Math.roundTo = function(n,step) {
	return step*Math.round(n/step);
}